<?php
    $server_name = "localhost";
    $db_username = "root";
    $db_password = "";
    $db_name = "kampus";

    $connection = mysqli_connect($server_name,$db_username,$db_password,$db_name);
    $dbconfig = mysqli_select_db($connection,$db_name);    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
    integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <title>Kampus Buah</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <br><h3 class="text-center">Data Mahasiswa</h3><br>
    <table class="m-auto content-table">
        <?php
            $query = "select m.nim, m.nama, m.alamat, m.jenis_kelamin, m.photos, p.nama_prodi
            from mahasiswa m, prodi p
            where m.id_prodi=p.id_prodi";
            $query_run = mysqli_query($connection, $query);
        ?>
        <thead>
            <tr>
            <th>NIM</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Jenis Kelamin</th>
            <th>Prodi</th>
            <th>Foto</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(mysqli_num_rows($query_run) > 0)
                {
                    while($row = mysqli_fetch_assoc($query_run))
                    {
                        ?>
            <tr>
                <td width="150"><?php echo $row['nim']; ?></td>
                <td width="150"><?php echo $row['nama']; ?></td>
                <td width="150"><?php echo $row['alamat']; ?></td>
                <td width="200"><?php echo $row['jenis_kelamin']; ?></td>
                <td width="150"><?php echo $row['nama_prodi']; ?></td>
                <td width="150"><img src="images/<?php echo $row['photos']; ?>" width="100"></td>
            </tr>
            <?php
                    }
                }
                
                else
                {
                    echo '<h2 style="color: #f0134d;"> Tidak Ada Data </h2>';
                }
                ?>
        </tbody>
    </table>

    <br><h3 class="text-center">Data Prodi</h3>
    <table class="m-auto content-table">
        <?php
            $query2 = "select * from prodi";
            $query_run2 = mysqli_query($connection, $query2);
        ?>
        <thead>
            <tr>
            <th>ID Prodi</th>
            <th>Nama Prodi</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(mysqli_num_rows($query_run2) > 0)
                {
                    while($row2 = mysqli_fetch_assoc($query_run2))
                    {
                        ?>
            <tr>
                <td width="150"><?php echo $row2['id_prodi']; ?></td>
                <td width="200"><?php echo $row2['nama_prodi']; ?></td>
            </tr>
            <?php
                    }
                }
                
                else
                {
                    echo '<h2 style="color: #f0134d;"> Tidak Ada Data </h2>';
                }
                ?>
        </tbody>
    </table>

    <br>

</body>
</html>